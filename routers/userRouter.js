const express = require('express');
const router = express.Router();
const {ifDriverFree} = require('../middlewares/checkIfDriverFree');
const {getUser, deleteUser, changePassword, changePhoto} = require('../controllers/userController');
const authMiddleware = require('../middlewares/authMiddleware');

router.all('/*', authMiddleware);
router.get('/me', getUser);
router.delete('/me', ifDriverFree, deleteUser);
router.patch('/me/password', ifDriverFree, changePassword);
router.patch('/me/photo', changePhoto);

module.exports = router;
