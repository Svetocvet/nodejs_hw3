const express = require('express');
const router = express.Router();

const {getLoads, createLoad, getActiveLoad, iterateToNextLoad, getLoad, updateLoad, deleteLoad, postLoad, getLoadShippingInfo} = require('../controllers/loadController');
const authMiddleware = require('../middlewares/authMiddleware');
const {driverRoleMiddleware, shipperRoleMiddleware} = require('../middlewares/roleMiddleware');

router.all('/*', authMiddleware);
router.get('/', getLoads);
router.post('/', shipperRoleMiddleware, createLoad);
router.get('/active', driverRoleMiddleware, getActiveLoad);
router.patch('/active/state', driverRoleMiddleware, iterateToNextLoad);
router.get('/:id', getLoad);
router.put('/:id', shipperRoleMiddleware, updateLoad);
router.delete('/:id', shipperRoleMiddleware, deleteLoad);
router.post('/:id/post', shipperRoleMiddleware, postLoad);
router.get('/:id/shipping_info', shipperRoleMiddleware, getLoadShippingInfo);

module.exports = router;
